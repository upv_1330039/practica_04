package course.examples.practica_04;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Resources recursos = getResources();

        TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("Tab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Grados");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("Tab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Area");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("Tab3");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Millas-Km");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);
    }

    public void gradosC(View view){
        Intent intent = new Intent(this, grados.class);
        EditText editText = (EditText) findViewById(R.id.gradosF);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void obtenerArea(View view){
        Intent intent = new Intent(this, areaCuadrado.class);
        EditText editText = (EditText) findViewById(R.id.lado);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void millasAKm(View view){
        Intent intent = new Intent(this, millasKm.class);
        EditText editText = (EditText) findViewById(R.id.millas);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}