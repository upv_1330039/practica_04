package course.examples.practica_04;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class millasKm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_millas_km);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double miles = Double.parseDouble(message);
        miles = miles/0.62137;

        TextView textView = new TextView(this);
        textView.setTextSize(25);
        textView.setText("El resultado es: "+miles+" Km.");

        setContentView(textView);
    }
}