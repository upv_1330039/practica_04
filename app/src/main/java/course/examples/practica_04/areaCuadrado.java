package course.examples.practica_04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class areaCuadrado extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_cuadrado);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double l = Double.parseDouble(message);
        l = (l*l);
        TextView textView = new TextView(this);
        textView.setTextSize(25);
        textView.setText("Area del Cuadrado: "+l);
        setContentView(textView);
    }
}